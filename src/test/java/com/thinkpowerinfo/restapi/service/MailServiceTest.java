//package com.thinkpowerinfo.restapi.service;
//
//import java.io.File;
//import java.io.IOException;
//
//import javax.mail.MessagingException;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//
///**
// * TODO: mock a testing SMTP server
// */
////@ActiveProfiles("test")
//@SpringBootTest
//@RunWith(SpringJUnit4ClassRunner.class)
//public class MailServiceTest {
//
//    @Autowired
//    private MailService mailService;
//    
//    @Before
//    public void init() {
//    }
//    
//    @Test(expected = Test.None.class)
//    public void testSendText() {
//        MailService.MailProp prop = new MailService.MailProp(
//                "tony.liu@thinkpower-info.com", "test", "test mail");
//        mailService.sendText(prop);
//    }
//    
//    @Test(expected = Test.None.class)
//    public void testSendMessage() throws MessagingException, IOException {
//        MailService.MailProp prop = new MailService.MailProp(
//                "tony.liu@thinkpower-info.com", "test", "test mail");
//        
//        File file = new File("abc.txt");
//        try {
//            if (!file.exists()) {
//                file.createNewFile();
//            }
//            mailService.sendTextWithAttathment(prop, file);
//        } catch (Exception ex) {
//            throw ex;
//        } finally {
//            file.delete();
//        }
//    }
//    
//}
