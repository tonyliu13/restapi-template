package com.thinkpowerinfo.restapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thinkpowerinfo.restapi.service.MailService;


@RestController
@RequestMapping("/mail-service")
public class MailController {

    @Autowired
    private MailService mailService;

    /**
     * POST /restapi/mail-service HTTP/1.1
     *   Content-Disposition: form-data; name="to"
     *   "tony.liu@thinkpower-info.com"
     *   Content-Disposition: form-data; name="subject"
     *   "test subject"
     *   Content-Disposition: form-data; name="msg"
     *   "abcd 1234"
     */
    @RequestMapping(method = {RequestMethod.POST})
    public void sendMail(@RequestParam("to") String to,
            @RequestParam("subject") String subject,
            @RequestParam("msg") String msg) {
        mailService.sendText(to, subject, msg);
    }

}
