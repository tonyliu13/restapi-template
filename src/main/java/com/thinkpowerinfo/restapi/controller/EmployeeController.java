package com.thinkpowerinfo.restapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.thinkpowerinfo.restapi.entity.Employee;
import com.thinkpowerinfo.restapi.service.EmployeeService;

/**
 * TODO: 需新增，1. validation，2. authentication
 */
@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private EmployeeService empService;

    /**
     * GET http://localhost:8080/restapi/employees
     */
    @RequestMapping(method = {RequestMethod.GET})
    public List<Employee> findAll() {
        return empService.findAll();
    }
    
    /**
     * GET http://localhost:8080/restapi/employees/1
     */
    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    public Employee findById(@PathVariable("id") long id) {
        return empService.findById(id);
    }
    
    /**
     * POST http://localhost:8080/restapi/employees
     *   Header -> Content-Type:application/json;charset=utf-8
     *   Body   -> {"firstName":"Joe", "lastName":"Chen", "title":"Engineer", "startDate":"2019-10-22"}
     */
    @RequestMapping(method = {RequestMethod.POST})
    public Employee addEmp(@RequestBody(required = true) Employee emp) {
        return empService.save(emp);
    }
    
    /**
     * PUT http://localhost:8080/restapi/employees/6
     *   Header -> Content-Type:application/json;charset=utf-8
     *   Body   -> {"title":"Sales"}
     */
    @RequestMapping(value = "/{id}", method = {RequestMethod.PUT})
    public Employee updateEmp(@PathVariable("id") long id, @RequestBody(required = true) Employee emp) {
        return empService.update(id, emp);
    }
    
    /**
     * DELETE http://localhost:8080/restapi/employees/10
     */
    @RequestMapping(value = "/{id}", method = {RequestMethod.DELETE})
//    public void removeEmp(@PathVariable("id") long id) {
    public String removeEmp(@PathVariable("id") long id) {
        boolean isDel = empService.remove(id);
        return (isDel ? "EMP_ID = " + id + " had removed" : "Remove not finished!");
    }

}
