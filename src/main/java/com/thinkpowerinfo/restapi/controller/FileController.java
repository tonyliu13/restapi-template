package com.thinkpowerinfo.restapi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.thinkpowerinfo.restapi.service.FileService;

/**
 * http://localhost:8080/restapi/file.html
 */
@RestController
@RequestMapping("/file-storage")
public class FileController {

    @Autowired
    private FileService fileService;

    /**
     * POST http://localhost:8080/restapi/file
     *      multipart/form-data; boundary=----WebKitFormBoundaryxPUhzlJCCYKfVQto
     */
//    @RequestMapping(value = "/file", method = {RequestMethod.POST})
    @PostMapping("/file")
    public FileResponse uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        String fileName = fileService.saveFile(file);

        String downloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/file-storage/files/")
                .path(fileName)
                .toUriString();
        return new FileResponse(fileName, downloadUri, file.getContentType(), file.getSize());
    }

    /**
     * POST http://localhost:8080/restapi/files
     *      multipart/form-data; boundary=----WebKitFormBoundaryVX0zTxAJIEm5HMgL
     */
//    @RequestMapping(value = "/files", method = {RequestMethod.POST})
    @PostMapping("/files")
    public List<FileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) throws IOException {
        /* TODO: how to exception handle in lambda exception */
//        return Arrays.asList(files)
//                .stream()
//                .map(file -> uploadFile(file))
//                .collect(Collectors.toList());
        List<FileResponse> results = new ArrayList<>();
        for (MultipartFile file : files) {
            results.add(uploadFile(file));
        }
        return results;
    }

//    @RequestMapping(value = "/files/{fileName}", method = {RequestMethod.GET})
//    @GetMapping("/files/{fileName}")
    @GetMapping("/files/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName,
            HttpServletRequest request) throws IOException {
        Resource resource = fileService.loadUploadFile(fileName);

        /* Try to determine file's content type */
        String contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }


    @SuppressWarnings("unused")
    private static class FileResponse {
        private String fileName;
        private String fileDownloadUri;
        private String fileType;
        private long size;

        FileResponse(String fileName, String fileDownloadUri, String fileType, long size) {
            this.fileName = fileName;
            this.fileDownloadUri = fileDownloadUri;
            this.fileType = fileType;
            this.size = size;
        }

        public String getFileName() {
            return fileName;
        }
        public void setFileName(String fileName) {
            this.fileName = fileName;
        }
        public String getFileDownloadUri() {
            return fileDownloadUri;
        }
        public void setFileDownloadUri(String fileDownloadUri) {
            this.fileDownloadUri = fileDownloadUri;
        }
        public String getFileType() {
            return fileType;
        }
        public void setFileType(String fileType) {
            this.fileType = fileType;
        }
        public long getSize() {
            return size;
        }
        public void setSize(long size) {
            this.size = size;
        }
    }

}
