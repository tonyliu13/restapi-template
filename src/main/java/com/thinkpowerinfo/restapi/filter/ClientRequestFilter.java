package com.thinkpowerinfo.restapi.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The filter are registered by default for all the URL's
 * 
 * If you want a filter to only apply to certain URL patterns.
 *   1. remove the @Component
 *   2. to register the filter using a FilterRegistrationBean in a @Configuration
 *   
 *     @Bean
 *     public FilterRegistrationBean<RequestClientFilter> requestFilter() {
 *         FilterRegistrationBean<RequestClientFilter> filterBean = new FilterRegistrationBean<>();
 *         filterBean.setFilter(new RequestClientFilter());
 *         filterBean.addUrlPatterns("/user/*");
 *         return filterBean;    
 *     }
 */
//@Component
//@Order(1000)
public class ClientRequestFilter extends HttpFilter {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientRequestFilter.class);
    
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res,
            FilterChain chain) throws IOException, ServletException {
        LOGGER.info("client = " + req.getLocalAddr());
        super.doFilter(req, res, chain);
    }

}
