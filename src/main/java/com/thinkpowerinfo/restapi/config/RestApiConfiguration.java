//package com.thinkpowerinfo.restapi.config;
//
//import org.springframework.boot.web.server.WebServerFactoryCustomizer;
//import org.springframework.boot.web.servlet.FilterRegistrationBean;
//import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import com.thinkpowerinfo.restapi.filter.ClientRequestFilter;
//
//
//@Configuration
//public class RestApiConfiguration {
//
//    /**
//     * Spring Boot 2 how to set context path
//     *
//     *   1. application.properties
//     *     server.servlet.context-path=/restapi
//     *
//     *   2. Java System Property
//     *     public static void main(String[] args) {
//     *         System.setProperty("server.servlet.context-path", "/restapi");
//     *         SpringApplication.run(RestApiApplication.class, args);
//     *     }
//     *
//     *   3. register a WebServerFactoryCustomizer
//     *
//     *     WebServerFactoryCustomizer<ConfigurableServletWebServerFactory>
//     *     WebServerFactoryCustomizer<TomcatServletWebServerFactory>
//     *     WebServerFactoryCustomizer<JettyServletWebServerFactory >
//     *     WebServerFactoryCustomizer<UndertowServletWebServerFactory >
//     *
//     * ps. priority
//     *    WebServerFactoryCustomizer > Java System Properties > application.properties
//     */
//    @Bean
//    public WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> webServerFactoryCustomizer() {
////        return new WebServerFactoryCustomizer<ConfigurableServletWebServerFactory>() {
////            @Override
////            public void customize(ConfigurableServletWebServerFactory factory) {
////                factory.setContextPath("/restapi");
////            }
////        };
//        return factory -> factory.setContextPath("/restapi");
//    }
//
//    /**
//     * filter for /file-storage
//     */
//    @Bean
//    public FilterRegistrationBean<ClientRequestFilter> clientRequestFilter() {
//        FilterRegistrationBean<ClientRequestFilter> filterBean = new FilterRegistrationBean<>();
//        filterBean.setFilter(new ClientRequestFilter());
//        filterBean.addUrlPatterns("/file-storage/*");
//        return filterBean;
//    }
//
//}
