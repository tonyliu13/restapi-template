package com.thinkpowerinfo.restapi.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@EnableJpaRepositories("com.thinkpowerinfo.restapi.dao")
@Configuration
public class SpringDataJpaConfiguration {

    @Autowired
    Environment propEnv;

    @Bean
    DataSource dataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName(propEnv.getProperty("spring.datasource.driverClassName"));
        ds.setUrl(propEnv.getProperty("spring.datasource.url"));
        ds.setUsername(propEnv.getProperty("spring.datasource.username"));
        ds.setPassword(propEnv.getProperty("spring.datasource.password"));
        return ds;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean lfb = new LocalContainerEntityManagerFactoryBean();
        lfb.setDataSource(dataSource());
        lfb.setPersistenceProviderClass(org.hibernate.jpa.HibernatePersistenceProvider.class);
        lfb.setPackagesToScan("com.thinkpowerinfo.restapi.entity");

        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", propEnv.getProperty("spring.jpa.hibernate.dialect"));
        properties.setProperty("hibernate.show_sql", propEnv.getProperty("spring.jpa.hibernate.show-sql", "false"));
        lfb.setJpaProperties(properties);
        return lfb;
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

}
