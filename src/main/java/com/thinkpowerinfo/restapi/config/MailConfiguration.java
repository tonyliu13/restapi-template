package com.thinkpowerinfo.restapi.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class MailConfiguration {

    @ConfigurationProperties(prefix = "java.mail")
    @Bean
    public JavaMailSender javaMailSender(
            @Value("${java.mail.host}") String host,
            @Value("${java.mail.port}") String port,
            @Value("${java.mail.username}") String username,
            @Value("${java.mail.password}") String password) {
//        JavaMailSender mailSender = new JavaMailSenderImpl();
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
//        mailSender.setPort(port);
        mailSender.setPort(Integer.parseInt(port));
        mailSender.setUsername(username);
        mailSender.setPassword(password);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
//        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.smtp.socketFactory.port", "465");
//        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        props.put("mail.smtp.socketFactory.fallback", "false");
        mailSender.setJavaMailProperties(props);
        return mailSender;
    }

//    @Bean
//    public JavaMailSender javaMailSender(@Autowired MailProp mailProp) {
////        JavaMailSender mailSender = new JavaMailSenderImpl();
//        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//        mailSender.setHost(mailProp.getHost());
//        mailSender.setPort(mailProp.getPort());
//        mailSender.setUsername(mailProp.getUsername());
//        mailSender.setPassword(mailProp.getPassword());
//
//        Properties props = mailSender.getJavaMailProperties();
//        props.put("mail.transport.protocol", "smtp");
//        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.debug", "true");
////        props.put("mail.smtp.starttls.enable", "true");
////        props.put("mail.smtp.socketFactory.port", "465");
////        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
////        props.put("mail.smtp.socketFactory.fallback", "false");
//        mailSender.setJavaMailProperties(props);
//        return mailSender;
//    }
//
//    @Profile("default")
//    @ConfigurationProperties(prefix = "java.mail")
//    @Bean("mailProp")
//    public MailProp devMailProp() {
//        return new MailProp();
//    }
//
//    /**
//     * _單元測試，使用 @ActiveProfiles("test") 指定使用這個 mail property
//     * TODO: 測試的 SMTP server ??
//     */
//    @Profile("test")
//    @Bean("mailProp")
//    public MailProp testMailProp() {
//        MailProp prop = new MailProp();
//        return prop;
//    }
//
//    public static class MailProp {
//        private String host;
//        private int port;
//        private String username;
//        private String password;
//
//        public String getHost() {
//            return host;
//        }
//        public void setHost(String host) {
//            this.host = host;
//        }
//        public int getPort() {
//            return port;
//        }
//        public void setPort(int port) {
//            this.port = port;
//        }
//        public String getUsername() {
//            return username;
//        }
//        public void setUsername(String username) {
//            this.username = username;
//        }
//        public String getPassword() {
//            return password;
//        }
//        public void setPassword(String password) {
//            this.password = password;
//        }
//    }

}
