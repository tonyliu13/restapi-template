package com.thinkpowerinfo.restapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.thinkpowerinfo.restapi.entity.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long> {

}
