package com.thinkpowerinfo.restapi.exception;

public class RestApiFileNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public RestApiFileNotFoundException(String msg) {
        super(msg);
    }
    
}
