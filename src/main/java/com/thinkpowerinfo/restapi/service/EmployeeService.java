package com.thinkpowerinfo.restapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkpowerinfo.restapi.dao.EmployeeRepository;
import com.thinkpowerinfo.restapi.entity.Employee;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository empRepository;
    

    public Employee findById(long id) {
        return empRepository.findById(id).get();
    }

    public List<Employee> findAll() {
        return empRepository.findAll();
    }
    
    @Transactional
    public Employee save(Employee emp) {
        return empRepository.save(emp);
    }
    
    /*
     * _直接使用 client request 的 JSON 封裝的 emp 更新，若 field 為 null 也會把資料庫的值更新為 null
     * _若欄位有 NOT NULL 限制，會導致 JdbcSQLIntegrityConstraintViolationException: NULL not allowed for column ...
     * 
     * TODO: update 時若遇到 field 為 null 時，是否允許更新
     */
    @Transactional
    public Employee update(long id, Employee emp) {
        emp.setEmpId(id);
        return empRepository.save(emp);
    }
//    @Transactional
//    public Employee update(long id, Employee emp) {
//        Employee origEmp = findById(id);
//        // 使用 BeanUtils.copyProperties(...) 把 emp 要變更的值設到 origEmp，再更新 origEmp
//        
//        return empRepository.save(origEmp);
//    }
    
    /*
     * Spring JPA Repository 的 deleteById(ID id) 無回傳值，無法判斷是否成功刪除
     */
//    @Transactional
//    public void remove(long id) {
//        empRepository.deleteById(id);
//    }
    @Transactional
    public boolean remove(long id) {
        empRepository.deleteById(id);
        if (empRepository.existsById(id)) {
            return true;
        }
        return false;
    }

}
