package com.thinkpowerinfo.restapi.service;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

/**
 * org.springframework.mail.MailAuthenticationException:
 *   Authentication failed; nested exception is javax.mail.AuthenticationFailedException: 535-5.7.8 Username and Password not accepted.
 *
 * _若使用 gmail(smtp.gmail.com) 測試，必須在 [低安全性應用程式存取權] 開啟存取權
 * _開發或部署時，選擇可以使用的 SMTP server
 */
@Service
public class MailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);

    @Autowired
    private JavaMailSender mailSender;

    /**
     * send text message
     */
    public void sendText(String to, String subject, String msg) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(msg);
        try {
            mailSender.send(message);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    /**
     * send message with attachment
     */
    public void sendTextWithAttathment(String to, String subject,
            String msg, File file) throws MessagingException {
        MimeMessage mineMsg = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(mineMsg, true);
        } catch (MessagingException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }

        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(msg);

        FileSystemResource attach = new FileSystemResource(file);
        helper.addAttachment(attach.getFilename(), file);
        try {
            mailSender.send(mineMsg);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }


//    public static class MailProp {
//        private String mailTo;
//        private String subject;
//        private String content;
//
//        public MailProp(String mailTo, String subject, String content) {
//            super();
//            this.mailTo = mailTo;
//            this.subject = subject;
//            this.content = content;
//        }
//
//        public String getMailTo() {
//            return mailTo;
//        }
//        public String getSubject() {
//            return subject;
//        }
//        public String getContent() {
//            return content;
//        }
//    }

}
