package com.thinkpowerinfo.restapi.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.thinkpowerinfo.restapi.exception.RestApiFileNotFoundException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@Api(value = "restapi file service")
@ConfigurationProperties(prefix = "file.upload")
@Service
public class FileService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileService.class);

    /* where to save the uploaded file, @see application.properties */
    private String uploadPath;

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }
    public String getUploadPath() {
        return uploadPath;
    }


    /*
     * ==== Spring Boot initialization - start ====
     * create directory for "upload-path" configured in application.properties
     */
    private Path fileUploadPath;

    @PostConstruct
    public void init() {
        try {
            Files.createDirectories(Paths.get(uploadPath).toAbsolutePath().normalize());
        } catch (Exception ex) {
            LOGGER.error("create file stored directory fail!", ex);
            // TODO: exception handler
        }
        this.fileUploadPath = Paths.get(uploadPath).toAbsolutePath().normalize();
    }
    /* ==== Spring Boot initialization - finish ==== */


    /**
     * save uploaded file to system specified path
     */
    @ApiOperation(value = "save uploaded file to system specified path", response = String.class)
    public String saveFile(MultipartFile file) throws IOException {
        /* Normalize file name */
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            /* Copy file to the target location (Replacing existing file with the same name) */
            Path targetPath = this.fileUploadPath.resolve(fileName);

            file.transferTo(targetPath);
//            Files.copy(file.getInputStream(), targetPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            LOGGER.error("save file " + fileName + " fail!", ex);
            throw ex;
        }
        return fileName;
    }

    /**
     * load file from system specified path
     */
    @ApiOperation(value = "load file from system specified path", response = Resource.class)
    public Resource loadUploadFile(String fileName) throws MalformedURLException {
        Resource resource = null;
        try {
            Path targetPath = this.fileUploadPath.resolve(fileName).normalize();
            resource = new UrlResource(targetPath.toUri());
        } catch (MalformedURLException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
        if (resource == null || !resource.exists()) {
            String msg = "The file (" + fileName + ") is not found!";
            LOGGER.error(msg);
            throw new RestApiFileNotFoundException(msg);
        }
        return resource;
    }

}
