package com.thinkpowerinfo.restapi.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Date;

/**
 * 
 * com.thinkpowerinfo.restapi.aspect.ControllerExceptionHandler:31 - An Exception occurred!
 *   org.springframework.http.converter.HttpMessageConversionException: Type definition error: [simple type, class org.hibernate.proxy.pojo.bytebuddy.ByteBuddyInterceptor]; nested exception is com.fasterxml.jackson.databind.exc.InvalidDefinitionException: No serializer found for class org.hibernate.proxy.pojo.bytebuddy.ByteBuddyInterceptor and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationFeature.FAIL_ON_EMPTY_BEANS) (through reference chain: com.thinkpowerinfo.restapi.entity.Employee["superior"]->com.thinkpowerinfo.restapi.entity.Employee$HibernateProxy$nTSNSfWB["hibernateLazyInitializer"])
 *     at org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter.writeInternal(AbstractJackson2HttpMessageConverter.java:293)
 * 
 * _設定 @ManyToOne(fetch = FetchType.LAZY) 時，發生 HttpMessageConversionException
 * _因為 @ManyToOne(fetch = FetchType.LAZY)，hibernate 查詢的 Employee 物件，department 會是一個 org.hibernate.proxy.pojo.bytebuddy.ByteBuddyInterceptor@3867024d
 * _而 ByteBuddyInterceptor 無法被寫出 JSON 物件
 * 
 * _使用 @JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler", "fieldHandler"})
 * 
 * TODO: 查詢如何處理 @ManyToOne(fetch = FetchType.LAZY)
 */
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler", "fieldHandler"})
@JsonPropertyOrder({"empId", "firstName","lastName", "title", "department", "superior", "startDate", "endDate"})
@Entity
@NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	public Employee() {
    }

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMP_ID")
	private long empId;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "LAST_NAME")
	private String lastName;
	
	private String title;

	@Temporal(TemporalType.DATE)
	@Column(name = "START_DATE")
	private Date startDate;

	@Temporal(TemporalType.DATE)
    @Column(name = "END_DATE")
    private Date endDate;

	//bi-directional many-to-one association to Department
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "DEPT_ID")
	private Department department;

	//bi-directional many-to-one association to Employee
	/**
	 * TODO: fetch=FetchType.LAZY 無效，還是會去查 SUPERIOR_EMP_ID
	 */
//	@ManyToOne
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SUPERIOR_EMP_ID")
	private Employee superior;

	
    public long getEmpId() {
        return empId;
    }

    public void setEmpId(long empId) {
        this.empId = empId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Employee getSuperior() {
        return superior;
    }

    public void setSuperior(Employee superior) {
        this.superior = superior;
    }

}