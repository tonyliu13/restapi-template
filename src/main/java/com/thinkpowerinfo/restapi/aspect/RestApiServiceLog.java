package com.thinkpowerinfo.restapi.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * To log method starting and finish within service layer
 */
@Aspect
@Component
public class RestApiServiceLog {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestApiServiceLog.class);

    /*
     * autowired HttpServletRequest to get request information
     */
//    @Autowired
//    private HttpServletRequest request;


    @Pointcut("execution(* com.thinkpowerinfo.restapi.service..*(..))")
    public void serviceLayerAllMethod() {
    }

    @Before("serviceLayerAllMethod()")
    public void before(JoinPoint joinPoint) {
        String serviceName = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        LOGGER.info(serviceName + "." + methodName + " - start");
    }

    @After("serviceLayerAllMethod()")
    public void after(JoinPoint joinPoint) {
        String serviceName = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        LOGGER.info(serviceName + "." + methodName + " - finish");
    }

}
