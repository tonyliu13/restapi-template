package com.thinkpowerinfo.restapi.aspect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.thinkpowerinfo.restapi.exception.RestApiFileNotFoundException;
import com.thinkpowerinfo.restapi.service.FileService;

/**
 */
@ControllerAdvice
//public class ControllerExceptionHandler {
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    /**
     * all Exception that not handled
     */
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<String> handleDefaultExceptions(Exception ex, WebRequest request) {
        LOGGER.error("An Exception occurred!", ex);
//        List<String> details = new ArrayList<>();
//        details.add(ex.getLocalizedMessage());
        return new ResponseEntity<String>("We are sorry that server has an error!", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * java.lang.IllegalStateException: Ambiguous @ExceptionHandler method mapped for [class org.springframework.web.HttpRequestMethodNotSupportedException]:
     *   {public final org.springframework.http.ResponseEntity com.thinkpowerinfo.restapi.aspect.ControllerExceptionHandler.handleHttpMethod405(java.lang.Exception,org.springframework.web.context.request.WebRequest), public final org.springframework.http.ResponseEntity org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler.handleException(java.lang.Exception,org.springframework.web.context.request.WebRequest) throws java.lang.Exception}
     *
     * _Spring 的 ResponseEntityExceptionHandler 已經有處理 HttpRequestMethodNotSupportedException
     * _如果需要自行處理 HttpRequestMethodNotSupportedException，改為 override handleHttpRequestMethodNotSupported()
     */
//    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
//    public final ResponseEntity<String> handleHttpMethod405(Exception ex, WebRequest request) {
//        HttpRequestMethodNotSupportedException ex2 = (HttpRequestMethodNotSupportedException) ex;
//        final String msg = "We are sorry that " + ex2.getMethod() + " method is not allowed for the URL!";
//        return new ResponseEntity<String>(msg, HttpStatus.METHOD_NOT_ALLOWED);
//    }
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        final String msg = "We are sorry that " + ex.getMethod() + " method is not allowed for the URL!";
        return new ResponseEntity<Object>(msg, HttpStatus.METHOD_NOT_ALLOWED);
    }

    /**
     * @see FileService
     */
    @ExceptionHandler(RestApiFileNotFoundException.class)
    public final ResponseEntity<String> handleFileNotFound(Exception ex, WebRequest request) {
        LOGGER.error("File is not found!", ex);
        return new ResponseEntity<String>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    /**
     * the exception is thrown by Spring while checking multipart file upload
     */
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public final ResponseEntity<String> handleFileSizeLimitExceeded(Exception ex, WebRequest request) {
        LOGGER.error("Size of uploaded file is over size limited!", ex);
        return new ResponseEntity<String>("Size of uploaded file is over size limited!", HttpStatus.SERVICE_UNAVAILABLE);
    }

}
