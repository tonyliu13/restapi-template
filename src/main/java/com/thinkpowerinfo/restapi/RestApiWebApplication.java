package com.thinkpowerinfo.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * To extend SpringBootServletInitializer if deploy to external AP server like Tomcat, ... etc
 * SpringBootServletInitializer implement Servlet 3.0 SPI, will be detected automatically by SpringServletContainerInitializer, which itself is bootstrapped automatically by any Servlet 3.0 container
 * _如果要部署至外部 tomcat，則必須實做 SpringBootServletInitializer
 * _SpringBootServletInitializer 實做了 WebApplicationInitializer，而 WebApplicationInitializer 實現了 Servlet 3， AP server 啟動後會自動偵測部署的 web application
 *
 * _ps. notice to deploy to external Tomcat
 *   _RestApiConfiguration 設定 context path 只適用於 Spring Boot 2 的 embedded server
 *   _若要部署在外部 Tomcat，必須設定 conf/ 內的設定
 *   _或是直接以 .war 的名稱做為 context path
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.thinkpowerinfo.restapi"})
public class RestApiWebApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(RestApiWebApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(RestApiWebApplication.class, args);
    }

}
