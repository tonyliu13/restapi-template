drop table if exists EMPLOYEE;
drop table if exists DEPARTMENT;

create table DEPARTMENT (
    DEPT_ID integer not null auto_increment,
    NAME    varchar(20),
    primary key (DEPT_ID)
);

create table EMPLOYEE (
    EMP_ID             integer not null auto_increment,
    FIRST_NAME         varchar(20),
    LAST_NAME          varchar(20),
    TITLE              varchar(20),
    DEPT_ID            integer,
    SUPERIOR_EMP_ID    integer,
    START_DATE         date,
    END_DATE           date,
    primary key (EMP_ID)
);


alter table EMPLOYEE add constraint EMPLOYEE_DEPARTMENT_FK foreign key (DEPT_ID) references DEPARTMENT (DEPT_ID);
alter table EMPLOYEE add constraint EMPLOYEE_EMPLOYEE_FK foreign key (SUPERIOR_EMP_ID) references EMPLOYEE (EMP_ID);


-- department data  
insert into department (dept_id, name) values (null, 'Operations');
insert into department (dept_id, name) values (null, 'Loans');
insert into department (dept_id, name) values (null, 'Administration');
insert into department (dept_id, name) values (null, 'IT');


/* employee data */
insert into employee (emp_id, First_Name, Last_Name, start_date, dept_id, title) values (null, 'Michael', 'Smith', '2001-06-22', 
  (select dept_id from department where name = 'Administration'), 'President');
insert into employee (emp_id, First_Name, Last_Name, start_date, dept_id, title) values (null, 'Susan', 'Barker', '2002-09-12', 
  (select dept_id from department where name = 'Administration'), 'Vice President');
insert into employee (emp_id, First_Name, Last_Name, start_date, dept_id, title) values (null, 'Robert', 'Tyler', '2000-02-09',
  (select dept_id from department where name = 'IT'), 'Treasurer');
insert into employee (emp_id, First_Name, Last_Name, start_date, dept_id, title) values (null, 'Susan', 'Hawthorne', '2002-04-24', 
  (select dept_id from department where name = 'Operations'), 'Operations Manager');
insert into employee (emp_id, First_Name, Last_Name, start_date, dept_id, title) values (null, 'John', 'Gooding', '2003-11-14', 
  (select dept_id from department where name = 'Loans'), 'Loan Manager');
insert into employee (emp_id, First_Name, Last_Name, start_date, dept_id, title) values (null, 'Helen', 'Fleming', '2004-03-17', 
  (select dept_id from department where name = 'Operations'), 'Head Teller');
insert into employee (emp_id, First_Name, Last_Name, start_date, dept_id, title) values (null, 'Chris', 'Tucker', '2004-09-15', 
  (select dept_id from department where name = 'Operations'), 'Teller');

  
create temporary table emp_tmp as select emp_id, First_Name, Last_Name from employee;
update employee set superior_emp_id = (select emp_id from emp_tmp where Last_Name = 'Smith' and First_Name = 'Michael') where ((Last_Name = 'Barker' and First_Name = 'Susan') or (Last_Name = 'Tyler' and First_Name = 'Robert'));
update employee set superior_emp_id = (select emp_id from emp_tmp where Last_Name = 'Tyler' and First_Name = 'Robert') where Last_Name = 'Hawthorne' and First_Name = 'Susan';
update employee set superior_emp_id = (select emp_id from emp_tmp where Last_Name = 'Hawthorne' and First_Name = 'Susan') where ((Last_Name = 'Gooding' and First_Name = 'John') or (Last_Name = 'Fleming' and First_Name = 'Helen') or (Last_Name = 'Roberts' and First_Name = 'Paula')  or (Last_Name = 'Blake' and First_Name = 'John')  or (Last_Name = 'Markham' and First_Name = 'Theresa')); 
update employee set superior_emp_id = (select emp_id from emp_tmp where Last_Name = 'Fleming' and First_Name = 'Helen') where ((Last_Name = 'Tucker' and First_Name = 'Chris') or (Last_Name = 'Parker' and First_Name = 'Sarah') or (Last_Name = 'Grossman' and First_Name = 'Jane'));  
update employee set superior_emp_id = (select emp_id from emp_tmp where Last_Name = 'Roberts' and First_Name = 'Paula') where ((Last_Name = 'Ziegler' and First_Name = 'Thomas') or (Last_Name = 'Jameson' and First_Name = 'Samantha'));   
update employee set superior_emp_id = (select emp_id from emp_tmp where Last_Name = 'Blake' and First_Name = 'John') where ((Last_Name = 'Mason' and First_Name = 'Cindy') or (Last_Name = 'Portman' and First_Name = 'Frank'));    
update employee set superior_emp_id = (select emp_id from emp_tmp where Last_Name = 'Markham' and First_Name = 'Theresa') where ((Last_Name = 'Fowler' and First_Name = 'Beth') or (Last_Name = 'Tulman' and First_Name = 'Rick'));    
drop table emp_tmp;

commit;